package com.example.randomnumber;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    int number = 0;
    Random random;
    boolean isCorrectAnswer = false;
    int remanningTry = 5;

    TextView mRemainingTextView;
    Button mCheckButton;
    EditText mGuessNumberEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mRemainingTextView = findViewById(R.id.remaining_text_view);
        mCheckButton = findViewById(R.id.check_button);
        mGuessNumberEditText = findViewById(R.id.guess_number_edit_text);

        generateRandomNumber();
        remanningTry = Integer.parseInt(mRemainingTextView.getText().toString());

        mCheckButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (remanningTry == 1) {
                    isCorrectAnswer = checkCorrectAnswer(Integer.parseInt(mGuessNumberEditText.getText().toString()));
                    showDialogBox(isCorrectAnswer);
                } else {
                    isCorrectAnswer = checkCorrectAnswer(Integer.parseInt(mGuessNumberEditText.getText().toString()));
                    if (isCorrectAnswer) {
                        showDialogBox(true);
                    } else {
                        remanningTry -= 1;
                        mRemainingTextView.setText(String.valueOf(remanningTry));
                    }
                }
            }
        });
    }

    private void showDialogBox(boolean isCorrect) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        if (!isCorrect) {
            builder.setMessage(getString(R.string.dialog_massage_fail) + String.valueOf(number) + getString(R.string.try_again));
        } else {
            builder.setMessage(getString(R.string.dialog_massage_success) + getString(R.string.try_again));
        }
        builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                restApp();
            }
        });
        builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void restApp() {
        mGuessNumberEditText.setText(null);
        mRemainingTextView.setText("5");
        remanningTry = 5;
        isCorrectAnswer = false;
    }

    private boolean checkCorrectAnswer(int answer) {
        if (answer == number){
            return true;
        } else if (answer > number){
            Toast.makeText(getApplicationContext(), "Your Answer is greater than\nthe chosen Number" , Toast.LENGTH_LONG).show();
            return false;
        } else {
            Toast.makeText(getApplicationContext(), "Your Answer is less than\nthe chosen Number", Toast.LENGTH_LONG).show();
            return false;
        }
    }

    private void generateRandomNumber() {
        random = new Random();
        number = random.nextInt(100) + 1;
    }
}
